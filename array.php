<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>array</h2>
    <?php
        echo  "<h3> soal 1 </h3>";
        $Kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        print_r($Kids);
        echo "<br>";
        $adults = ["Hopper", "Nancy", "Joyce", "Jonathan", "Murray"];
        print_r($adults);


        echo "<h3> Soal 2</h3>";
        echo "Cast Stranger Things: ";
        echo "<br>";
        echo "Total Kids: ". count($Kids); // Berapa panjang array kids
        echo "<br>";
        echo "<ol>";
        echo "<li>" . $Kids[0] ."</li>";
        echo "<li>" . $Kids[1] ."</li>";
        echo "<li>" . $Kids[2] ."</li>";
        echo "<li>" . $Kids[3] ."</li>";
        echo "<li>" . $Kids[4] ."</li>";
        echo "<li>" . $Kids[5] ."</li>";
        echo "</ol>";


        echo "Total Adults: ". count($adults); // Berapa panjang array adults
        echo "<br>";
        echo "<ol>";
        echo "<li>". $adults[0] ."</li>";
        echo "<li>". $adults[1] ."</li>";
        echo "<li>". $adults[2] ."</li>";
        echo "<li>". $adults[3] ."</li>";
        echo "<li>". $adults[4] ."</li>";
        echo "</ol>";

        echo "<h3>soal 3</h3>";
        $biodata = [
            ["Name" => "Will Byers", "Age" => 12, "Aliases" => "Will the Wise", "Status" => "Alive"],
            ["Name" => "Mike Wheeler", "Age" => 12, "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["Name" => "Jim Hopper", "Age" => 43, "Aliases" => "Chief Hopper", "Status" => "Deceased"],
            ["Name" => "Eleven", "Age" => 12, "Aliases" => "El", "Status" => "Alive"]
        ];

        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
    ?>
    
    
</body>
</html>