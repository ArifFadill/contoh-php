<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh soal</h2>

    <?php
        echo "<h3>contoh 1</h3>";
        $kalimat1 = " PHP is never old ";
        echo "kalimat pertama :". $kalimat1. "<br>";
        echo "panjang string :". strlen($kalimat1). "<br>";
        echo "jumlah kata :". str_word_count($kalimat1). "<br>". "<br>";


        echo "<h3>contoh 2</h3>";
        $string2 = "I love PHP";
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: ". substr($string2, 2, 5);
        echo "<br> Kata Ketiga: ". substr($string2, 6, 8) ;

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but Good!";
        echo "string : ". $string3. "<br>";
        echo "kalimat yang di ganti : ". str_replace("Good", "awesome", $string3);
    ?>
</body>
</html>